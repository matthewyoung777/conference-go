from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_weather_data(city, state):
    coords_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(coords_url)
    data = json.loads(response.content)
    lat, lon = data[0]["lat"], data[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(weather_url)
    data = json.loads(response.content)
    return {
        "temp": data["main"]["temp"],
        "description": data["weather"][0]["description"],
    }


def get_photo(city):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}&per_page=1"
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    result = data["photos"][0]["url"]
    return {"picture_url": result}
